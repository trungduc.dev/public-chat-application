import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyDMLrCfaHHxZuTE1zKrpaVP3PgR6-LKiQ0",
    authDomain: "instant-message-38b4a.firebaseapp.com",
    databaseURL: "https://instant-message-38b4a.firebaseio.com",
    projectId: "instant-message-38b4a",
    storageBucket: "instant-message-38b4a.appspot.com",
    messagingSenderId: "925094641620",
    appId: "1:925094641620:web:d8b148b1a9d3d6af86a041"
  };

firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth;
export const db = firebase.database();
import React, { useEffect, useState } from "react";
import "./inboxCard.scss";
import { useHistory } from "react-router-dom";
import { db } from "../../../services/firebase";

const InboxCard = (props) => {
  const { users, rooms } = props;
  const history = useHistory();
  const currentUser = JSON.parse(localStorage.getItem("user"));
  const [roomMessages, setRoomMessages] = useState(undefined);

  useEffect(() => {
    let _roomMessages = [];
    db.ref(`chats/messages/personalRoom/`).on("value", (snapshot) => {
      snapshot.forEach(() => {
        _roomMessages.push(snapshot.val());
      });
      setRoomMessages(_roomMessages);
    });
  }, []);

  const showInbox = (room) => {
    const toUser = users.filter(
      (user) =>
        user.id ==
        room.participants.filter((user) => user !== currentUser.id)[0]
    )[0];
    history.push({
      pathname: `/inbox`,
      state: { currentUser: currentUser, toUser: toUser, room: room },
    });
  };

  return (
    // users
    // .filter(item => item.username !== currentUser.username)
    // .map(item => (
    //     <div key={item.username} className="inbox" onClick={() => showInbox(item)}>
    //         <p>{item.name}</p>
    //     </div>
    // ))
    rooms.map((room) => {
      if (users && users.length) {
            return (
              <div
                key={room.id}
                className="inbox"
                onClick={() => showInbox(room)}
              >
                <p>
                  {
                    users.filter(
                      (user) =>
                        user.id ==
                        room.participants.filter(
                          (user) => user !== currentUser.id
                        )[0]
                    ).map(user => {
                      return(user.isOnline? `${user.name} (Online)` : `${user.name} (Offline)`)
                    })
                  }
                </p>
              </div>
            );
          } else {
            return null;
          }
    })
  );
};

export default InboxCard;

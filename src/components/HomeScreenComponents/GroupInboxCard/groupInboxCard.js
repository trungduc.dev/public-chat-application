import { statement } from "@babel/template";
import React, { useState, useEffect } from "react";
import "./groupInboxCard.scss";
import { db } from "../../../services/firebase";
import { useHistory } from "react-router";

const GroupInboxCard = (props) => {
  let history = useHistory();
  const { users, currentUser } = props;

  const [componentStates, setComponentStates] = useState({
    createGroupForm: "none",
  });
  const [listUsers, setListUsers] = useState([]);
  const [groupRooms, setGroupRooms] = useState([]);

  useEffect(() => {
    db.ref(`room/groupRoom`).on("value", (snapshot) => {
      let _groupRooms = [];
      snapshot.forEach((snap) => {
        if (snap.val().participants.includes(currentUser.id)) {
          _groupRooms.push(snap.val());
        }
      });
      setGroupRooms(_groupRooms);
    });
  }, []);

  const showGroupInbox = (room) => {
    console.log(room.id);
    history.push(`/group/${room.id}`);
  };
  const showCreateGroupChat = () => {
    setComponentStates({
      ...componentStates,
      createGroupForm: "",
    });
  };
  const closeCreateGroupChat = () => {
    setComponentStates({
      ...componentStates,
      createGroupForm: "none",
    });
    setListUsers([]);
  };
  const handleSelectUser = (user) => {
    console.log("select user: ", user.name);
    if (!listUsers.includes(user.id)) {
      setListUsers((prevList) => {
        return [...prevList, ...[user.id]];
      });
    } else {
      let newListUsers = listUsers.filter((item) => item !== user.id);
      setListUsers(newListUsers);
    }
  };

  const createGroupChat = () => {
    let id = new Date().getTime().toString();
    listUsers.push(currentUser.id);
    if (listUsers.length < 3) {
        window.alert("The member should be more than 3")
    } else {
      let room = {
        id: id,
        participants: listUsers,
      };
      db.ref("room").child("groupRoom").push(room);
      closeCreateGroupChat();
      setListUsers([])
    }
  };
  return (
    <div id="groupInboxCard">
      <div
        id="createGroupChatContainer"
        style={{ display: componentStates.createGroupForm }}
      >
        <div id="createGroupChatForm">
          <div id="createGroupChat_header">
            <p onClick={() => closeCreateGroupChat()}>Button</p>
          </div>
          <div id="createGroupChat_content">
            {users
              .filter((user) => user.id !== currentUser.id)
              .map((user) => {
                let filter = "";
                if (listUsers.includes(user.id)) {
                  filter = "selected";
                }
                return (
                  <div
                    key={user.id}
                    id="userCards"
                    className={filter}
                    onClick={() => handleSelectUser(user)}
                  >
                    <p>{user.name}</p>
                  </div>
                );
              })}
          </div>
          <div id="createGroupChat_footer">
            <p onClick={() => createGroupChat()}>Create</p>
          </div>
        </div>
      </div>
      <div className="addGroupContainer" onClick={() => showCreateGroupChat()}>
        <p>Add</p>
      </div>
      {groupRooms.map((room) => (
        <div className="groupInbox" onClick={() => showGroupInbox(room)}>
          <p>{room.id}</p>
        </div>
      ))}

      <script></script>
    </div>
  );
};

export default GroupInboxCard;

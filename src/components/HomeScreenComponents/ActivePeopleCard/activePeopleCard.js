import React from "react";
import "./activePeopleCard.scss";
import { useHistory } from "react-router-dom";
import { db } from "../../../services/firebase";

const ActivePeopleCard = (props) => {
  const { users, rooms } = props;
  const history = useHistory();
  const currentUser = JSON.parse(localStorage.getItem("user"));

  const showInbox = (user) => {
    let id = new Date().getTime().toString();
    let room = rooms.filter((room) => room.participants.includes(user.id));
    console.log("co room: ", room);

    history.push({
      pathname: `/inbox`,
      state: { currentUser: currentUser, toUser: user, room: room[0] },
    });
  };

  return (
    <>
      <div
        className="userIconContainer"
        onClick={() => history.push('/upload')}
      >
        <p>Add story</p>
      </div>

      {users
        .filter((user) => user.id !== currentUser.id)
        .map((user) => {
          const status = user.isOnline ? "Online" : "Offline";
          return (
            <div
              key={user.id}
              className="userIconContainer"
              onClick={() => showInbox(user)}
            >
              <p>{user.name}</p>
              <p>{status}</p>
            </div>
          );
        })}
    </>
  );
};

export default ActivePeopleCard;

import React, { useState, useMemo, useRef } from "react";
import "./signupForm.scss";
import { db } from "../../services/firebase";

const SignupForm = (props) => {
  const { setSignUpState, accounts } = props;
  const [formState, setFormState] = useState({
    username: "",
    password: "",
    name: "",
  });
  const [warningStatus, setWarningStatus] = useState(false);

  const useFocus = () => {
    const htmlElRef = useRef(null)
    const setFocus = () => {htmlElRef.current &&  htmlElRef.current.focus()}

    return [ htmlElRef, setFocus ] 
  }

  const [inputRef, setInputFocus] = useFocus()

  const handleChange = (e) => {
    const { name, value } = e.target;

    console.log(value);

    setFormState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const res = accounts.filter(
      (account) => account.username == formState.username
    );
    if (res && res.length) {
      setWarningStatus(true);
      setFormState({
        username: formState.username,
        password: "",
        name: "",
      });
      setInputFocus()
    } else {
      let id = new Date().getTime().toString();
      let user = {
        id: id,
        username: formState.username,
        password: formState.password,
        name: formState.name,
      };
      db.ref("users/").push(user);
      setFormState({
        username: "",
        password: "",
        name: "",
      });
      setSignUpState(false);
    }
  };

  return (
    <div id="signupContainer">
      <form autoComplete="false" id="signupForm" onSubmit={handleSubmit}>
        <span>
          <label className="loginLabel" for="username">
            Username:{" "}
          </label><br></br>
          <input
            ref={inputRef}
            className="signupInput"
            required
            type="text"
            name="username"
            placeholder="Username"
            value={formState.username}
            onChange={handleChange}
          ></input>
        </span>

        <span>
          <label className="loginLabel" for="password">
            Password:{" "}
          </label><br></br>
          <input
            className="signupInput"
            required
            type="text"
            name="password"
            placeholder="Password"
            value={formState.password}
            onChange={handleChange}
          ></input>
        </span>

        <span>
          <label className="loginLabel" for="name">
            Your name:{" "}
          </label><br></br>
          <input
            className="signupInput"
            required
            type="text"
            name="name"
            placeholder="Your name"
            value={formState.name}
            onChange={handleChange}
          ></input>
        </span>
        {warningStatus ? <p id="warningText">The username has existed !!</p> : ""}
        <button id="signupBtn" type="submit">
          Signup
        </button>
        <div id="loginTextContainer">
          <p>Already have account?</p>
          <p id="loginText" onClick={() => setSignUpState(false)}>
            Login
          </p>
        </div>
      </form>
    </div>
  );
};

export default SignupForm;

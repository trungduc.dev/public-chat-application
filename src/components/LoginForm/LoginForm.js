import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./loginForm.scss";
import { db } from "../../services/firebase";

const LoginForm = (props) => {
  const { accounts, setSignUpState } = props;
  // Using useHistory to routing pages
  const history = useHistory();

  const [_formState, _setFormState] = useState({
    username: "",
    password: "",
  });

  // When user submit, authenticate user and then redirect to home page if valid
  const handleLogin = (e) => {
    e.preventDefault();
    let result = accounts.filter(
      (item) =>
        item.username === _formState.username &&
        item.password === _formState.password
    );

    // If the user is authenticated, redirect to the homepage
    if (result && result.length) {
      db.ref(`users`).once("value", (snapshot) => {
        snapshot.forEach((snap) => {
          if (snap.val().username === result[0].username) {
            const key = snap.key;
            db.ref(`users/` + key).update({ isOnline: true });
          }
        });
      });
      localStorage.setItem("user", JSON.stringify(result[0]));
      console.log(JSON.stringify(result[0]));
      history.push(`/home`);
    } else window.location.reload();
  };

  // setState for username || password when the input changed
  const handleChange = (e) => {
    const { name, value } = e.currentTarget;
    _setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <form autoComplete="off" id="loginForm" onSubmit={handleLogin}>
      <span>
        <label className="loginLabel" for="username">Username: </label><br></br>
        <input
          required
          placeholder={"Username"}
          value={_formState.username}
          onChange={handleChange}
          type="text"
          name="username"
          value={_formState.username}
        />
      </span>
      <span>
        <label className="loginLabel" for="password">Password: </label><br></br>
        <input
          required
          placeholder={"Password"}
          value={_formState.password}
          onChange={handleChange}
          type="password"
          name="password"
        />
      </span>
      <button type="submit" id="loginBtn">
        Login
      </button>
      <div id="signupTextContainer">
        <p>Don't have account?</p>
        <p id="signupText" onClick={() => setSignUpState(true)}>Signup</p>
      </div>
    </form>
  );
};

export default LoginForm;

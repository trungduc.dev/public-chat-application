import React, { useState, useMemo, useEffect } from "react";
//import '../styles/chatScreen.scss'
import { db } from "../services/firebase";

const ChatInput = () => {

  const [text, setText] = useState("");

  const handleChange = (e) => {
    let messages = e.target.value;
    setText(messages);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    let id = new Date().getTime().toString();
    let content = { id: id, text: text };
    db.ref("chats/").child("messages").push(content);
    setText("");
  };

  const input = useMemo(
    () => <input onChange={handleChange} value={text} type="text" />,
    [text]
  );



  return (
    <form onSubmit={handleSubmit}>
      {input}
      <button type="submit">Send</button>
    </form>
  );
};

export default ChatInput;

import React, { useState, useEffect, useMemo } from "react";
import { useHistory, useParams } from "react-router-dom";
import "./groupChatScreen.scss";
import { db } from "../../services/firebase";

const GroupChatScreen = (props) => {
  const { roomID } = useParams();
  const [typing, setTyping] = useState('')
  const currentUser = JSON.parse(localStorage.getItem("user"));

  // Hooks initialize
  const [text, setText] = useState("");
  const [messages, setMessages] = useState([]);
  // const [typing, setTyping] = useState([]);

  useEffect(() => {
    // Connect to firebase
    db.ref(`chats/messages/groupRoom/${roomID}`).on("value", (snapshot) => {
      let _messages = [];
      snapshot.forEach((snap) => {
        _messages.push(snap.val());
      });
      setMessages(_messages);
    });

    const key = getRoomKey();

    console.log(`room/groupRoom/${key}/typing`)

    db.ref(`room/groupRoom/${key}/typing`).on("value", (snapshot) => {
      const userIds = snapshot.val()
      const users = []

      console.log('asod8uas8ed', userIds)

      for (const uid in userIds) {
        const u = userIds[uid];
        console.log('u:', u)
        if (uid !== currentUser.id && uid !== 'available') {
          users.push(u);
        }
      }

      setTyping(users.length ? (users.join(', ') + ' is typing ...') : '') 

      console.log('AAAAA', users)
      // let newItem = [];
      // Object.keys(snapshot.val()).map((item2) => {
      //   if (!isNaN(item2)) {
      //     newItem.push({ [item2]: item[item2] });
      //   }
      // });
      // console.log(newItem);

    });
  }, []);

  // Auto focus to the newest messages whenever the user redirect to personal inbox
  useEffect(() => {
    // const lastestMessage = document.getElementsByClassName(
    //   "messagesContainer"
    // )[0].lastChild;
    // const messagesContainer = document.getElementsByClassName(
    //   "messagesContainer"
    // )[0];
    // if (lastestMessage) {
    //   messagesContainer.scrollTop = lastestMessage.offsetTop;
    // }
  }, [messages]);

  // Functions initialize
  const handleChange = (e) => {
    setText(e.currentTarget.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let id = new Date().getTime().toString();
    let createdAt = new Date().toLocaleTimeString("en-US", {
      hour12: false,
      hour: "numeric",
      minute: "numeric",
    });

    let messages = {
      id: id,
      text: text,
      room_id: roomID,
      createdAt: createdAt,
      sender: currentUser.id,
    };
    db.ref(`chats/messages/groupRoom/${roomID}`).push(messages);
    setText("");
  };

  //   const getMessages = (allMess) => {
  //     for(let i = 0; i < allMess.length; i++)
  //     {
  //         return (
  //             <li className="userGroupMessages">
  //               hi
  //             </li>
  //           );
  //     }
  //   };

  const getRoomKey = () => {
    let key = undefined;
    db.ref(`room/groupRoom`).on("value", (snapshot) => {
      snapshot.forEach((snap) => {
        if (roomID == snap.val().id) {
          key = snap.key;
        }
      });
    });
    return key;
  };

  const handleOnFocus = () => {
    const key = getRoomKey();
    console.log('focus')
    db.ref(`room/groupRoom/${key}/typing`).update({
      [currentUser.id]: currentUser.name,
    });
  };

  const handleOnBlur = () => {
    const key = getRoomKey();
    db.ref(`room/groupRoom/${key}/typing`).once("value", (snapshot) => {
      let newData = snapshot.val();
      delete newData[currentUser.id];
      db.ref(`room/groupRoom/${key}/typing`).set(newData);
      setTyping('')
    });
  };

  // Other variables initialize
  const input = useMemo(
    () => <input onChange={handleChange} value={text} type="text" />,
    [text]
  );

  let history = useHistory();

  return (
    <div className="groupChatContainer">
      <div id="headerGroupChatScreen">
        <div>
          <h2
            onClick={() =>
              history.replace({
                pathname: "/home",
                state: {
                  groupChat: true,
                },
              })
            }
          >
            Back
          </h2>
        </div>
      </div>
      <div className="groupMessagesContainer">
        {messages.map((item, index) => {
          if (item.sender === currentUser.id) {
            if (index == 0 && messages[index + 1].sender == currentUser.id) {
              return (
                <li
                  id={item.id}
                  className="userGroupMessages_first"
                  key={item.id}
                >
                  {item.text}
                </li>
              );
            } else if (item.sender !== messages[index - 1].sender) {
              return (
                <li
                  id={item.id}
                  className="userGroupMessages_first"
                  key={item.id}
                >
                  {item.text}
                </li>
              );
            } else {
              return (
                <li id={item.id} className="userGroupMessages" key={item.id}>
                  {item.text}
                </li>
              );
            }
          } else {
            return (
              <li className="groupMessages" key={item.id}>
                {item.text}
              </li>
            );
          }
        })}
        {/* {typing.length ? typing.filter(item => item != null || Object.keys(item) != currentUser.id || Object.keys(item) != "available").map(item1 => {
              if(item1.hasOwnProperty("available")) {
                delete item1.available
                let newItem = item1
                console.log(newItem)
                return (<li>{Object.values(newItem) + " is typing"}</li>)
              }
            }) : null} */}


            <div>{typing}</div>

        {/* {typing.length
          ? typing.map((item) => {
              let newItem = [];
              Object.keys(item).map((item2) => {
                if (!isNaN(item2)) {
                  newItem.push({ [item2]: item[item2] });
                }
              });
              console.log(newItem);
              newItem.map((finalItem) => console.log(finalItem + " is typing"));
            })
          : null} */}

        {/* TEST */}
        {/* {messages.map((item, index) => {
            
            // Handle self messages
          if (item.sender === currentUser.id) {

            if(index == 0) {
                // Base condition 
                if(messages[index + 1] == undefined) {
                    return
                } else {
                    if(messages[index + 1].sender == currentUser.id)
                    {
                        return
                    } else {
                        return
                    }
                }
                
            }

            if(index != 0) {
                // Base condition 
                if(messages[index + 1] == undefined) {
                    return
                } else {
                    if(messages[index - 1].sender == currentUser.id && messages[index + 1].sender == currentUser.id) {
                        return
                    }
                    else if(messages[index - 1].sender == currentUser.id && messages[index + 1].sender != currentUser.id) {
                        return
                    }
                    else if(messages[index - 1].sender != currentUser.id && messages[index + 1].sender == currentUser.id) {
                        return
                    }
                    else {
                        return
                    }
                }
            }



            // // Bin
            // if (index == 0 && messages[index + 1].sender == currentUser.id) {
            //   return (
            //     <li
            //       id={item.id}
            //       className="userGroupMessages_first"
            //       key={item.id}
            //     >
            //       {item.text}
            //     </li>
            //   );
            // } else if (item.sender !== messages[index - 1].sender) {
            //   return (
            //     <li
            //       id={item.id}
            //       className="userGroupMessages_first"
            //       key={item.id}
            //     >
            //       {item.text}
            //     </li>
            //   );
            // } else {
            //   return (
            //     <li id={item.id} className="userGroupMessages" key={item.id}>
            //       {item.text}
            //     </li>
            //   );
            // }


            // Handle other messages
          } else {
            return (
              <li className="groupMessages" key={item.id}>
                {item.text}
              </li>
            );
          }
        })} */}
        {/* TEST */}

        {/* {getMessages(messages)} */}
      </div>
      <form
        id="chatInput"
        onBlur={() => handleOnBlur()}
        onFocus={() => handleOnFocus()}
        onSubmit={handleSubmit}
      >
        {input}
        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default GroupChatScreen;

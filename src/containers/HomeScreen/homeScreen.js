import React, { useState, useMemo, useEffect } from "react";
import "./homeScreen.scss";
import { db } from "../../services/firebase";

import ActivePeopleCard from "../../components/HomeScreenComponents/ActivePeopleCard/activePeopleCard";
import InboxCard from "../../components/HomeScreenComponents/InboxCard/inboxCard";
import ChatInput from "../../components/chatInput";
import GroupInboxCard from "../../components/HomeScreenComponents/GroupInboxCard/groupInboxCard";
import { useHistory } from "react-router-dom";

const HomeScreen = (props) => {
  const currentUser = JSON.parse(localStorage.getItem("user"));
  const [conversation, setConversation] = useState([]);
  const [allUsers, setAllUsers] = useState([]);
  const [personalRooms, setPersonalRooms] = useState([]);
  const [groupChat, setGroupChat] = useState(false);
  let history = useHistory();

  useEffect(() => {
    db.ref("chats/messages").on("value", (snapshot) => {
      let allConversation = [];
      snapshot.forEach((snap) => {
        allConversation.push(snap.val());
      });
      setConversation(allConversation);
    });

    db.ref("users/").on("value", (snapshot) => {
      let _allUsers = [];
      snapshot.forEach((snap) => {
        _allUsers.push(snap.val());
      });
      setAllUsers(_allUsers);
    });

    db.ref("room/personalRoom").on("value", (snapshot) => {
      let _personalRooms = [];
      snapshot.forEach((snap) => {
        if (snap.val().participants.includes(currentUser.id)) {
          _personalRooms.push(snap.val());
        }
      });
      setPersonalRooms(_personalRooms);
    });

    if (props.location.state) {
      setGroupChat(props.location.state);
    }
    return () => {};
  }, []);

  const handleLogout = () => {
    history.replace("/");
    db.ref(`users`).once("value", (snapshot) => {
      snapshot.forEach(snap => {
        if(snap.val().username === currentUser.username) {
          const key = snap.key
          db.ref(`users/`+ key).update({isOnline: false})
        }
      })
    });
    localStorage.removeItem("user");
  };

  const handleInboxContainer = () => {
    console.log("ok");
  };

  return (
    <div className="container">
      <div className="header">
        <h2>User: {currentUser.name}</h2>
        <div onClick={() => handleLogout()}>
          <h3>Logout</h3>
        </div>
      </div>

      <div className="content">
        <div className="activePeople">
          <ActivePeopleCard
            users={allUsers}
            rooms={personalRooms}
          ></ActivePeopleCard>
        </div>
        <div className="inboxContainer">
          <div className="inboxHeader">
            <h2 onClick={() => setGroupChat(false)}>Messages</h2>
            <h2 onClick={() => setGroupChat(true)}>Groups</h2>
          </div>
          <div className="inboxCard">
            {!groupChat ? (
              <InboxCard
                users={allUsers}
                currentUser={currentUser}
                rooms={personalRooms}
              ></InboxCard>
            ) : (
              <GroupInboxCard
                users={allUsers}
                currentUser={currentUser}
              ></GroupInboxCard>
            )}
          </div>
        </div>
      </div>

      <div className="footer"></div>
      {/* 
      <div>
        {conversation.map((item) => (
          <li>{item.text}</li>
        ))}
      </div>
      <ChatInput></ChatInput> */}
    </div>
  );
};

export default HomeScreen;

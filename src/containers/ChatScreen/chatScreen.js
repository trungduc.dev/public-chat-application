import React, { useState, useMemo, useEffect } from "react";
import "./chatScreen.scss";
import { db } from "../../services/firebase";
import { useHistory } from "react-router-dom";

const ChatScreen = (props) => {
  const [text, setText] = useState("");
  const [rooms, setAllRooms] = useState([]);
  const [messages, setMessages] = useState([]);
  const [tracking, setTracking] = useState(undefined);
  const currentUser = JSON.parse(localStorage.getItem("user"));
  let currentRoom = props.location.state.room;
  const toUser = props.location.state.toUser;
  const toUserStatus = toUser.isOnline

  if (!currentRoom) {
    [currentRoom] = rooms.filter(
      (room) =>
        room.participants.includes(currentUser.id) &&
        room.participants.includes(toUser.id)
    );
  }

  let history = useHistory();

  useEffect(() => {
    //Tracking all the rooms
    db.ref(`room/personalRoom`).on("value", (snapshot) => {
      let allRooms = [];
      snapshot.forEach((snap) => {
        allRooms.push(snap.val());
      });
      setAllRooms(allRooms);
    });

    // Tracking all the messages in the current room
    if (currentRoom) {
      if (!tracking) {
        db.ref(`chats/messages/personalRoom/${currentRoom.id}`).on(
          "value",
          (snapshot) => {
            let _messages = [];
            snapshot.forEach((snap) => {
              _messages.push(snap.val());
            });
            setMessages(_messages);
          }
        );
      } else {
        db.ref(`chats/messages/personalRoom/${tracking}`).on(
          "value",
          (snapshot) => {
            let _messages = [];
            snapshot.forEach((snap) => {
              _messages.push(snap.val());
            });
            setMessages(_messages);
          }
        );
      }
    }
  }, [tracking]);

  // Auto focus to the newest messages whenever the user redirect to personal inbox
  useEffect(() => {
    const lastestMessage = document.getElementsByClassName(
      "messagesContainer"
    )[0].lastChild;
    const messagesContainer = document.getElementsByClassName(
      "messagesContainer"
    )[0];
    if (lastestMessage) {
      messagesContainer.scrollTop = lastestMessage.offsetTop;
    }
  }, [messages]);

  const handleChange = (e) => {
    let _text = e.target.value;
    setText(_text);
  };

  const pushMessages = (id, createdAt) => {
    let messages = {
      id: id,
      text: text,
      room_id: id,
      createdAt: createdAt,
      sender: props.location.state.currentUser.id,
    };
    db.ref(`chats/messages/personalRoom/${id}`).push(messages);
    setText("");
    setTracking(id);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // if(rooms[0].includes(props.location.state.toUser.username)
    //  && rooms[0].includes("duc")){
    //     //push messages to messages collection
    //     let id = new Date().getTime().toString();
    //     let messages = { id: id, text: text };
    // }

    // Retrieve the current room information
    let room = undefined;
    if (currentRoom) {
      room = rooms.filter((item) => item.id === currentRoom.id);
    }
    // Generate id and created time of the messages
    let id = new Date().getTime().toString();
    let createdAt = new Date().toLocaleTimeString("en-US", {
      hour12: false,
      hour: "numeric",
      minute: "numeric",
    });

    // Check if the room is existed or not
    if (room && room.length) {
      // If the room exist, create new messages and push to the room's messages storage
      let messages = {
        id: id,
        text: text,
        room_id: room[0].id,
        createdAt: createdAt,
        sender: props.location.state.currentUser.id,
      };
      db.ref(`chats/messages/personalRoom/${currentRoom.id}`).push(messages);
      setText("");
    } else {
      // If the room is not exist, create a new one and then push the messages to it
      let room = {
        id: id,
        participants: [currentUser.id, toUser.id],
      };
      db.ref("room").child("personalRoom").push(room);
      pushMessages(id, createdAt);
    }
  };

  const input = useMemo(
    () => (
      <input
        id="messagesInput"
        placeholder={"Aa"}
        required
        onChange={handleChange}
        value={text}
        type="text"
        name="text"
      />
    ),
    [text]
  );

  return (
    <div className="container">
      <div id="headerChatScreen">
        <div id="headerBackBtn" onClick={() => history.replace("/home")}>
          <i class="fas fa-angle-left"></i>
        </div>
        <div id="headerContentContainer">
          <div id="toUserContainer">
            <p>{toUser.name}</p>
            {/* <div id="statusContainer"> */}
              {/* <i class="fas fa-circle"></i> */}
              <p id="onlineStatus">{toUserStatus ? "Online" : "Offline"}</p>
            {/* </div> */}
          </div>
          <div id="toUserInfo">
            <i class="fas fa-info-circle"></i>
          </div>
        </div>
      </div>
      <div className="messagesContainer">
        {messages.map((item) => {
          if (item.sender === currentUser.id) {
            return (
              <li id={item.id} className="userMessages" key={item.id}>
                {item.text}
              </li>
            );
          } else
            return (
              <li className="messages" key={item.id}>
                {item.text}
              </li>
            );
        })}
      </div>
      <form autoComplete="false" id="chatInput" onSubmit={handleSubmit}>
        {input}
        <button id="sendMessagesBtn" type="submit">
          <i class="far fa-paper-plane"></i>
        </button>
      </form>
    </div>
  );
};

export default ChatScreen;

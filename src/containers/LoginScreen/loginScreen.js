import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";

import './Login.scss'
import LoginForm from '../../components/LoginForm/LoginForm'
import SignupForm from '../../components/SignupForm/signupForm'

import { db } from '../../services/firebase'

const LoginScreen = () => {
  const [accounts, setAccounts] = useState(undefined)
  const [signUpState, setSignUpState] = useState(false)

  const history = useHistory()

  if(localStorage.getItem("user")) {
    history.push('/home')
  }
  
  useEffect(()=> {
    db.ref("users/").once("value", snapshot => {
        const allAccounts = []
        snapshot.forEach( snap => {
            allAccounts.push(snap.val())
        })
        console.log(allAccounts)
        setAccounts(allAccounts)
      });
  }, [])

  const getForm = () => {
    if(!signUpState) {
      return(
          <LoginForm accounts={accounts} setSignUpState={setSignUpState}></LoginForm>
      )
    } else return (
      <SignupForm accounts={accounts} setSignUpState={setSignUpState}></SignupForm> 
    )
  }

    return (
      <div className="container">
        <h1 id="logoText">iChat messages</h1>
        <img alt="logo" id="logoImg" src="https://freeiconshop.com/wp-content/uploads/edd/chat-outline-filled.png"></img>
        {getForm()}
      </div>
    );
  };

  export default LoginScreen
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./uploadScreen.scss";

const UploadScreen = () => {
  const history = useHistory();

  const [imgSource, setImgSource] = useState(undefined);
  const [imgHistory, setImgHistory] = useState([]);

  const upload = () => {
    console.log("Uploaded");
  };
  const undoChanges = () => {
    let _imgHistory = imgHistory;
    if (_imgHistory && _imgHistory.length > 1) {
      let history = _imgHistory.pop();
      history.ctx.drawImage(
        history.img,
        history.x,
        history.y,
        history.img.width * history.scale,
        history.img.height * history.scale
      );
      setImgHistory(_imgHistory);
    } else if (!_imgHistory.length) {
      alert("Empty");
    } else {
      let history = _imgHistory[0];
      history.ctx.drawImage(
        history.img,
        history.x,
        history.y,
        history.img.width * history.scale,
        history.img.height * history.scale
      );
      setImgHistory(_imgHistory);
    }
  };
  const handleEffect = (e) => {
    const filter = e.currentTarget.id;

    let myCanvas = document.getElementById("myCanvas"); // Creates a canvas object
    let myContext = myCanvas.getContext("2d");
    let imgData = myContext.getImageData(
      0,
      0,
      myContext.canvas.width,
      myContext.canvas.height
    );
    let originData = undefined;
    let image = undefined;
    let horizontalArray = undefined;
    let processedArray = undefined;
    let finalArray = undefined;
    let separated1PixelArray = undefined;
    let pixels;

    const getProcessedArray = () => {
      originData = imgData.data;

      image = {
        width: myCanvas.width,
        height: myCanvas.height,
      };
      separated1PixelArray = [];
      for (let i = 0; i < originData.length; i += 4) {
        let tempArray = [];
        for (let e = i; e < i + 4; e++) {
          tempArray.push(originData[e]);
        }
        separated1PixelArray.push(tempArray);
      }

      let _processedArray = [];
      for (let i = 0; i < separated1PixelArray.length; i += image.width) {
        let tempArray2 = [];
        for (let e = i; e < i + image.width; e++) {
          tempArray2.push(separated1PixelArray[e]);
        }
        _processedArray.push(tempArray2);
      }

      return _processedArray;
    };

    switch (filter) {
      case "grayScale":
        pixels = imgData.data;

        for (var i = 0; i < pixels.length; i += 4) {
          let lightness = parseInt(
            (3 * pixels[i] + 4 * pixels[i + 1] + pixels[i + 2]) >>> 3
          );

          pixels[i] = lightness;
          pixels[i + 1] = lightness;
          pixels[i + 2] = lightness;
        }
        myContext.putImageData(imgData, 0, 0);
        console.log(pixels)
        break;

      case "flipHorizontal":
        processedArray = getProcessedArray();

        for (let i = 0; i < processedArray.length; i++) {
          processedArray[i].reverse();
        }

        finalArray = processedArray.flat(3);
        imgData.data.set(finalArray);

        myContext.putImageData(imgData, 0, 0);

        break;

      case "flipVertical":
        processedArray = getProcessedArray();

        processedArray.reverse();
        finalArray = processedArray.flat(3);

        imgData.data.set(finalArray);
        myContext.putImageData(imgData, 0, 0);
        break;
      case "brighten":
        pixels = imgData.data;
        for (let i = 0; i < pixels.length; i += 4) {
          pixels[i] += 25;
          pixels[i + 1] += 25;
          pixels[i + 2] += 25;
        }
        imgData.data.set(pixels);
        myContext.putImageData(imgData, 0, 0);

        break;

      case "invert":
        pixels = imgData.data;
        for (let i = 0; i < pixels.length; i += 4) {
          pixels[i] = pixels[i] ^ 255;
          pixels[i + 1] = pixels[i + 1] ^ 255;
          pixels[i + 2] = pixels[i + 2] ^ 255;
        }
        imgData.data.set(pixels);
        myContext.putImageData(imgData, 0, 0);

        break;

      case "rotateRight":
        // let ang = 0;
        // let img = new Image();

        processedArray = getProcessedArray();

        image = {
          width: myCanvas.width,
          height: myCanvas.height,
        };
        // console.log(image.width)

        finalArray = [];
        for (let i = 0; i < image.width; i++) {
          let temp = [];
          for (let e = 0; e < image.height; e++) {
            temp.unshift(processedArray[e][i]);
          }
          finalArray.push(temp);
        }

        finalArray = finalArray.flat(3);
        imgData.data.set(finalArray);
        myContext.putImageData(imgData, 0, 0);

        break;
      case "rotateLeft":
        processedArray = getProcessedArray();

        finalArray = [];
        for (let i = 0; i < image.width; i++) {
          let temp = [];
          for (let e = 0; e < image.height; e++) {
            temp.push(processedArray[i][processedArray[i].length]);
          }
          finalArray.push(temp);
        }
        console.log(finalArray);

        finalArray = finalArray.flat(3);
        imgData.data.set(finalArray);
        myContext.putImageData(imgData, 0, 0);
        break;

      default:
        break;
    }
  };

  const handleImgChange = (e) => {
    let imageFile = e.target.files[0]; //here we get the image file
    let reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onloadend = function (e) {
      let myImage = new Image(); // Creates image object
      setImgSource(e.target.result);
      myImage.src = e.target.result; // Assigns converted image to image object
      myImage.onload = function (ev) {
        let myCanvas = document.getElementById("myCanvas"); // Creates a canvas object
        let myContext = myCanvas.getContext("2d"); // Creates a contect object

        var scale = Math.min(
          myCanvas.width / myImage.width,
          myCanvas.height / myImage.height
        );
        var x = myCanvas.width / 2 - (myImage.width / 2) * scale;
        var y = myCanvas.height / 2 - (myImage.height / 2) * scale;

        // myCanvas.width = myImage.width; // Assigns image's width to canvas
        // myCanvas.height = myImage.height; // Assigns image's height to canvas
        myContext.drawImage(
          myImage,
          x,
          y,
          myImage.width * scale,
          myImage.height * scale,
        ); // Draws the image on canvas
        let _imgHistory = imgHistory;
        _imgHistory.push({
          ctx: myContext,
          img: myImage,
          x: x,
          y: y,
          scale: scale,
        });
        setImgHistory(_imgHistory);
        let imgData = myCanvas.toDataURL("image/jpeg", 0.75); // Assigns image base64 string in jpeg format to a variable
      };
    };
  };

  return (
    <div id="uploadScreenContainer">
      <div id="uploadScreen_header">
        <p onClick={() => history.push("/home")}>Back</p>
        <p onClick={() => upload()}>Upload to stories</p>
      </div>
      <div id="uploadScreen_body">
        <div id="uploadScreen_body_button">
          <p>Img</p>
          <p onClick={undoChanges}>Undo</p>
        </div>
        <div id="uploadScreen_body_content">
          <canvas
            id="myCanvas"
            width="100%"
            height="100%"
            style={{ backgroundColor: "red" }}
          >
            Your browser does not support the HTML5 canvas tag.
          </canvas>
        </div>
      </div>
      <div id="uploadScreen_footer">
        <input
          onChange={handleImgChange}
          id="uploadFile"
          type="file"
          accept="image/*"
        ></input>
        <button onClick={handleEffect} id="grayScale">
          Gray Scale
        </button>
        <button onClick={handleEffect} id="flipHorizontal">
          Flip horizontal
        </button>
        <button onClick={handleEffect} id="flipVertical">
          Flip vertical
        </button>
        <button onClick={handleEffect} id="brighten">
          Brighten
        </button>
        <button onClick={handleEffect} id="invert">
          Invert
        </button>
        <button onClick={handleEffect} id="rotateRight">
          Rotate right
        </button>
        <button onClick={handleEffect} id="rotateLeft">
          Rotate left
        </button>
      </div>
    </div>
  );
};

export default UploadScreen;

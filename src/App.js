import logo from './logo.svg';
import React, { Component } from 'react';
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";

import HomeScreen from './containers/HomeScreen/homeScreen';
import LoginScreen from './containers/LoginScreen/loginScreen';
import ChatScreen from './containers/ChatScreen/chatScreen'
import GroupChatScreen from './containers/GroupChatScreen/groupChatScreen'
import UploadScreen from './containers/UploadScreen/uploadScreen'

import { auth } from './services/firebase';

import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={LoginScreen} />
        <Route path="/home" exact component={HomeScreen} />
        <Route path="/inbox" exact component={ChatScreen} />
        <Route path="/group/:roomID" exact component={GroupChatScreen} />
        <Route path="/upload" exact component={UploadScreen} />
      </Switch>
    </Router>
  );
}

export default App;

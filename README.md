# Real-time Chat Application
The real-time chat application project that deployed on website and publish to desktop app.

## Used technologies/techniques: 
- [x] Reactjs
- [x] Javascript
- [x] HTML/SCSS
- [x] Firebase Realtime Database
- [x] Wireframe (figma.com)

## Interface

##### Login page
<!-- ![Front-end screenshots](/public/images/Screenshot1.jpg) -->
<br/>
(In development)

## FEATURES FOR USERS

- [x] Login, logout
- [x] Send realtime messages
- [x] Create group chats
- [x] Join public rooms

#### Note:
- Using node-sass package version @4.14.1
- Using OTP code to login (admin only)

